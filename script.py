import fileinput
import re
NUMBER_CHAR = '*'
DOT_CHAR    = '.'
BULLET_CHARS = [ NUMBER_CHAR, DOT_CHAR]

class BulletDoc:
  def __init__(self):
    super(BulletDoc, self).__init__()
    self.number_data = {}
    self.bullet_data = {}
    self.last_indent = 0
    self.doc = []

  def calculate_prefixes(self, previous, level):
    removed_key = level + 1
    while not(self.number_data.pop(removed_key, None) is None):
      removed_key += 1

    if (len(previous) == 0):
      upper_level = self.number_data.get((level - 1), None)
      if (upper_level is None) or (len(upper_level) == 0): return ([1] * level)
      new_prefix = upper_level.copy()
      new_prefix.append(1)
      return new_prefix

    new_prefix = previous.copy()
    new_prefix[len(previous) - 1] += 1
    return new_prefix

  def add_line_to_doc(self, line):
    if not(line and line.strip()): return

    for test_char in BULLET_CHARS:
      match_exp = "^(\\{char})+".format(char = test_char)
      match_data = re.match(match_exp, line)
      if (match_data is None): continue

      bullets = match_data.group()
      level   = len(bullets)

      number_data = self.number_data.get(level, [])
      if (test_char == NUMBER_CHAR):
        new_number_data = self.calculate_prefixes(number_data, level)
        self.number_data[level] = new_number_data
        self.doc.append('.'.join(map(str, new_number_data)) + match_data.string[match_data.end():])
        self.last_indent = 0
        return

      if (level == 1):
        self.bullet_data = {}
      else:
        previous_bullet = self.bullet_data[level-1]
        self.doc[previous_bullet['index']] = previous_bullet['prefix'] + '+' + previous_bullet['content']

      self.doc.append(' ' * (level + 1) + '-' + match_data.string[match_data.end():])
      self.bullet_data[level] = { 'index': (len(self.doc) - 1), 'prefix': (' ' * (level + 1)), 'content': match_data.string[match_data.end():] }
      self.last_indent = level + 3
      return
    # BULLET_CHARS.each
    self.doc.append(' ' * self.last_indent + line)


  def result_doc(self):
    return self.doc


bullet_doc = BulletDoc()
with fileinput.input() as f_input:
  for line in  f_input:
    bullet_doc.add_line_to_doc(line.rstrip('\r\n'))

for line in  bullet_doc.result_doc():
  print(line)