class BulletDoc
  NUMBER_CHAR = '*'
  DOT_CHAR    = '.'
  BULLET_CHARS = [ NUMBER_CHAR, DOT_CHAR]

  def initialize
    @number_data = {}
    @bullet_data = {}
    @last_indent = 0
    @doc = []
  end

  def calculate_prefixes(previous:, level:)
    @number_data.delete_if {| key, value | key > level }
    if previous.empty?
      return (previous = [1] * level) if @number_data[level - 1].nil? || @number_data[level - 1].empty?
      return @number_data[level - 1].clone << 1
    end
    previous[previous.size - 1] += 1
    previous
  end

  def add_line_to_doc(line)
    return unless line.match(/\S/)

    BULLET_CHARS.each do |char|
      match_data = line.match(Regexp.compile("^(\\#{char})+"))
      next unless match_data

      bullets = match_data.to_s
      level   = bullets.length

      number_data = (@number_data[level] ||= [])
      if char == NUMBER_CHAR
        new_number_data = calculate_prefixes(previous: number_data, level: level)
        @number_data[level] = new_number_data
        @doc <<  "#{new_number_data.join('.')}#{match_data.post_match}"
        @last_indent = 0
        return
      end

      if level == 1
        @bullet_data = {}
      else
        previous_bullet = @bullet_data[level-1]
        @doc[previous_bullet[:index]] = previous_bullet[:prefix] + '+' + previous_bullet[:content]
      end

      @doc <<  "#{' ' * (level + 1)}-#{match_data.post_match}"
      @bullet_data[level] = { index: (@doc.size - 1), prefix: (' ' * (level + 1)), content: match_data.post_match }
      @last_indent = level + 3
      return
    end # BULLET_CHARS.each
    @doc <<  "#{' ' * @last_indent}#{line}"
  end

  def result_doc
    @doc
  end
end

bullet_doc = BulletDoc.new
ARGF.each do |line|
  bullet_doc.add_line_to_doc(line.chomp)
end

puts bullet_doc.result_doc.join("\n")